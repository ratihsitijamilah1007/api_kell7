/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.19-MariaDB : Database - project_kel_7
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_kel_7` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `project_kel_7`;

/*Table structure for table `data barang` */

DROP TABLE IF EXISTS `data barang`;

CREATE TABLE `data barang` (
  `kode_barang` char(5) NOT NULL,
  `nama_barang` varchar(25) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data barang` */

/*Table structure for table `data pengajuan` */

DROP TABLE IF EXISTS `data pengajuan`;

CREATE TABLE `data pengajuan` (
  `kode_pengajuan` char(5) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `npm_peminjam` int(8) DEFAULT NULL,
  `nama_peminjam` varchar(20) DEFAULT NULL,
  `prodi` varchar(20) DEFAULT NULL,
  `no_hp` int(12) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data pengajuan` */

/*Table structure for table `data pengajuan detail` */

DROP TABLE IF EXISTS `data pengajuan detail`;

CREATE TABLE `data pengajuan detail` (
  `kode_pengajuan` char(5) NOT NULL,
  `kode_barang` char(5) NOT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`,`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data pengajuan detail` */

/*Table structure for table `data pengembalian` */

DROP TABLE IF EXISTS `data pengembalian`;

CREATE TABLE `data pengembalian` (
  `kode_pengembalian` char(5) NOT NULL,
  `kode_pengajuan` char(5) NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`,`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data pengembalian` */

/*Table structure for table `data pengembalian detail` */

DROP TABLE IF EXISTS `data pengembalian detail`;

CREATE TABLE `data pengembalian detail` (
  `kode_pengembalian_detail` char(5) NOT NULL,
  `kode_pengembalian` char(5) NOT NULL,
  `kode_barang` char(5) NOT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian_detail`,`kode_pengembalian`,`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data pengembalian detail` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
